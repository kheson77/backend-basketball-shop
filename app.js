require('dotenv').config();
const express = require('express');
const app = express();

const productRouter = require('./api/product/product.router');
const categoryRouter = require('./api/categoryProduct/category.router');
const userRouter = require('./api/user/user.router');
const cartRouter = require('./api/cart/cart.router');
const newRouter = require('./api/news/news.router');

const connection = require('./config/database');

connection.getConnection((err) => {
    if(err){
        console.log(err);
        throw err;
    } else{
        console.log("connected mysql");
    }
});

app.use(express.json());

app.use('/api/product', productRouter);
app.use('/api/category', categoryRouter);
app.use('/api/user', userRouter);
app.use('/api/cart', cartRouter);
app.use('/api/news', newRouter);
app.use('/uploads', express.static('uploads'));

app.get('/', (req, res) => {
    res.send("hello world");
});

var port = process.env.PORT || 3000;
app.listen(port, () => console.log('Server up and running at ' + `${port}`));
