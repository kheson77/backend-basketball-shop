const router = require('express').Router();

const upload = require('../.././image/image');
const { checkToken } = require("../../auth/token_validation");

const {
    addUser, getUser, getUserById, updateUser, updateImageUser, deleteUser, login
} = require('./user.controller');

router.post('/register', addUser);
router.get('/', checkToken, getUser);
router.get('/:id',checkToken, getUserById);
router.patch('/:id', checkToken, updateUser);
router.patch('/updateImage/:id', checkToken, upload.single('userThumbnail'), updateImageUser);
router.delete('/:id', checkToken, deleteUser);
router.post('/login', login);

module.exports = router;