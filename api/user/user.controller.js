const { hashSync, genSaltSync, compareSync } = require("bcrypt");
const { sign } = require("jsonwebtoken");
const connection = require('../../config/database');

module.exports = {
    getUser: (req, res) => {
        var sql = "SELECT idUser,firstName,lastName,email,userThumbnail,gender,phoneNumber,modifiedDate,userType FROM User";
        var query = connection.query(sql, (err, result) => {
            if (err) throw err;
            res.json({
                count: result.length,
                next: null,
                previous: null,
                results: result
            });
        });
    },
    getUserById: (req, res) => {
        var idUser = req.params.id;
        var sql = "SELECT idUser,firstName,lastName,email,userThumbnail,gender,phoneNumber,modifiedDate,userType FROM User WHERE idUser=" + idUser;
        var query = connection.query(sql, (err, result) => {
            if (err) throw err;
            res.json(
                result[0]
            );
        });
    },
    updateUser: (req, res) => {
        var idUser = req.params.id;
        var data ;
        if(req.body.userType == null){
            data = {
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                gender: req.body.gender,
                phoneNumber: req.body.phoneNumber
            };
        } else{
            data = {
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                gender: req.body.gender,
                phoneNumber: req.body.phoneNumber,
                userType: req.body.userType
            };
        }
       
        var sql = "UPDATE User SET ? WHERE idUser=" + idUser;
        var query = connection.query(sql, data, (err, result) => {
            if (err) throw err;
            res.json({
                reponse: "Update info successfully",
                results: result
            });
        });
    },
    updateImageUser: (req, res) => {
        var idUser = req.params.id;
        var data = {
            userThumbnail: req.file.path
        };
        var sql = "UPDATE User SET ? WHERE idUser=" + idUser;
        var query = connection.query(sql, data, (err, result) => {
            if (err) throw err;
            res.json({
                reponse: "Update image successfully",
                results: result
            });
        })
    },
    addUser: (req, res) => {
        const salt = genSaltSync(10);
        req.body.password = hashSync(req.body.password, salt);

        var data = {
            email: req.body.email,
            password: req.body.password,
            lastName: req.body.lastName,
            userType: "customer"
        };

        var sql = "INSERT INTO User SET ?";
        var query = connection.query(sql, data, (err, result) => {
            if (err) throw err;
            res.json({
                reponse: "Add user successfully",
                results: result
            });
        });
    },
    deleteUser: (req, res) => {
        var idUser = req.params.id;
        var sql = "DELETE FROM User WHERE idUser=" + idUser;
        var query = connection.query(sql, (err, result) => {
            if (err) throw err;
            res.json({
                reponse: "Delete successfully",
                results: result
            });
        });
    },
    login: (req, res) => {
        const body = req.body;
        var sql = "SELECT * FROM User WHERE email=?";
        var query = connection.query(sql, body.email, (err, result) => {
            console.log(result[0]);
            if (err) {
                console.log(err);
                throw err;
            }
            if (!result[0]) {
                return res.status(400).json({
                    success: 0,
                    result: "Invalid email..."
                });
            }
            const checkToken = compareSync(body.password, result[0].password);
            if (checkToken) {
                result[0].password = undefined;
                const jsontoken = sign({ result: result[0] }, process.env.JWT_KEY, {
                    expiresIn: "1h"
                });
                return res.json({
                    idUser: result[0].idUser,
                    firstName: result[0].firstName,
                    lastName: result[0].lastName,
                    email: result[0].email,
                    userThumbnail: result[0].userThumbnail,
                    gender: result[0].gender,
                    phoneNumber: result[0].phoneNumber,
                    modifiedDate: result[0].modifiedDate,
                    userType: result[0].userType,
                    access_token: jsontoken
                });
            } else {
                return res.status(400).json({
                    success: 0,
                    result: "Invalid password..."
                });
            }
        });
    }
}