const connection = require('../../config/database');

module.exports = {
    getProduct: (req, res) => {
        var sql = "SELECT * FROM Product WHERE status=\"active\"";
        var query = connection.query(sql, (err, result) => {
            if (err) throw err;
            res.json({
                count: result.length,
                next: null,
                previous: null,
                results: result
            });
        });
    },
    getAllProduct: (req, res)=>{
        var sql = "SELECT * FROM Product";
        var query = connection.query(sql, (err, result) => {
            if (err) throw err;
            res.json({
                count: result.length,
                next: null,
                previous: null,
                results: result
            });
        });
    },
    getProductById: (req, res) => {
        var productId = req.params.id;
        var sql = "SELECT * FROM Product WHERE idProduct=" + productId;
        var query = connection.query(sql, (err, result) => {
            if (err) throw err;
            res.json({
                count: result.length,
                next: null,
                previous: null,
                results: result
            });
        });
    },
    getProductByCateName: (req, res) => {
        var cateName = req.params.cateName;
        var sql = "SELECT Product.* FROM Product INNER JOIN CategoryProduct ON Product.idCategory = CategoryProduct.idCategoryProduct WHERE CategoryProduct.nameCategory=? AND status=\"active\"";
        var query = connection.query(sql, cateName, (err, result) => {
            if (err) throw err;
            res.json({
                count: result.length,
                next: null,
                previous: null,
                results: result
            });
        });
    },
    getProductByCateId: (req, res) => {
        var cateId = req.params.cateId;
        var sql = "SELECT * FROM Product WHERE idCategory=" + cateId;
        var query = connection.query(sql, (err, result) => {
            if (err) throw err;
            res.json({
                count: result.length,
                next: null,
                previous: null,
                results: result
            });
        });
    },
    addProduct: (req, res) => {
        var data;
        // console.log(req.file);
        if (req.file == null) {
            data = {
                name: req.body.name,
                price: req.body.price,
                thumbnail: req.body.thumbnail,
                description: req.body.description,
                count: req.body.count,
                status: req.body.status,
                idCategory: req.body.idCategory
            };
        }
        else {
            data = {
                name: req.body.name,
                price: req.body.price,
                thumbnail: req.file.path,
                description: req.body.description,
                count: req.body.count,
                status: req.body.status,
                idCategory: req.body.idCategory
            };
        }

        var sql = "INSERT INTO Product SET ?";
        var query = connection.query(sql, data, (err, result) => {
            if (err) throw err;
            res.json({
                reponse: "Add successfully",
                results: result
            });
        });
    },
    updateProduct: (req, res) => {
        var productId = req.params.id;
        var data;
        if (req.file == null) {
            var data = {
                name: req.body.name,
                price: req.body.price,
                thumbnail: req.body.thumbnail,
                description: req.body.description,
                count: req.body.count,
                status: req.body.status,
                idCategory: req.body.idCategory
            };
        } else {
            data = {
                name: req.body.name,
                price: req.body.price,
                thumbnail: req.file.path,
                description: req.body.description,
                count: req.body.count,
                status: req.body.status,
                idCategory: req.body.idCategory
            };
        }

        var sql = "UPDATE Product SET ? WHERE idProduct=" + productId;
        var query = connection.query(sql, data, (err, result) => {
            if (err) throw err;
            res.json({
                reponse: "Update successfully",
                results: result
            });
        })
    },
    deleteProduct: (req, res) => {
        var productId = req.params.id;
        var sql = "DELETE FROM Product WHERE idProduct=" + productId;
        var query = connection.query(sql, (err, result) => {
            if (err) throw err;
            res.json({
                reponse: "Delete successfully",
                results: result
            });
        })
    },
    getRelatedProduct: (req, res) => {
        var productId = req.params.id;
        var sql = `SELECT * FROM Product WHERE idProduct NOT IN(${productId}) ORDER BY RAND() LIMIT 0,10`;
        var query = connection.query(sql, (err, result) => {
            if (err) throw err;
            res.json({
                count: result.length,
                next: null,
                previous: null,
                results: result
            });
        });
    }
};