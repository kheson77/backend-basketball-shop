const router = require('express').Router();

const upload = require('../.././image/image');
const { checkToken } = require("../../auth/token_validation");

const {
    getProduct, getAllProduct, getProductById, getProductByCateName,addProduct, getProductByCateId, updateProduct, deleteProduct, getRelatedProduct
} = require('./product.controller');

router.get('/', getProduct);
router.get('/all/',checkToken, getAllProduct);
router.post('/',checkToken,upload.single('thumbnail'), addProduct);
router.get('/byNameCate/:cateName', getProductByCateName);
router.get('/byIdCate/:cateId', getProductByCateId);
router.patch('/:id', checkToken,upload.single('thumbnail'),updateProduct);
router.delete('/:id', checkToken,deleteProduct);
router.get('/:id', getProductById);
router.get('/related/:id', getRelatedProduct);

module.exports = router;