const router = require('express').Router();

const {
    getNews, addNews, updateNews, deleteNews,getRelatedNews
} = require('./news.controller');

router.get('/', getNews);
router.post('/', addNews);
router.patch('/:id', updateNews);
router.delete('/:id', deleteNews);
router.get('/related/:id', getRelatedNews);

module.exports = router;