const connection = require('../../config/database');

module.exports = {
    getNews: (req, res) => {
        var sql = "SELECT * FROM News";
        var query = connection.query(sql, (err, result) => {
            if(err) throw err;
            res.json({
                count: result.length,
                next: null,
                previous: null,
                results: result
            });
        })
    },
    addNews: (req, res) => {
        var data = {
            title: req.body.title,
            thumbnail: req.body.thumbnail,
            shortContent: req.body.shortContent,
            content: req.body.content
        };
        var sql = "INSERT INTO News SET ?";
        var query = connection.query(sql, data, (err, result) => {
            if(err) throw err;
            res.json({
                reponse: "Add successfully",
                results: result
            });
        });
    },
    updateNews: (req, res) => {
        var idNews = req.params.id;
        var data = {
            title: req.body.title,
            thumbnail: req.body.thumbnail,
            shortContent: req.body.shortContent,
            content: req.body.content
        };
        var sql = "UPDATE News SET ? WHERE idNews=" + idNews;
        var query = connection.query(sql, data, (err, result) => {
            if(err) throw err;
            res.json({
                reponse: "Update successfully",
                results: result
            });
        })
    },
    deleteNews: (req, res) => {
        var idNews = req.params.id;
        var sql = "DELETE FROM News WHERE idNews=" + idNews;
        var query = connection.query(sql, (err, result) => {
            if(err) throw err;
            res.json({
                reponse: "Delete successfully",
                results: result
            });
        })
    },
    getRelatedNews: (req, res) => {
        var newsId = req.params.id;
        var sql =`SELECT * FROM News WHERE idNews NOT IN(${newsId}) ORDER BY RAND() LIMIT 0,10`;
        var query = connection.query(sql, (err, result) =>{
            if(err) throw err;
            res.json({
                count: result.length,
                next: null,
                previous: null,
                results: result
            });
        });
    }
}