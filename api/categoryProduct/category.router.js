const router = require('express').Router();

const {
    getCategory, addCategory, getCategoryById
} = require('./category.controller');

router.get('/', getCategory);
router.post('/', addCategory);
router.get('/:id', getCategoryById);

module.exports = router;