const connection = require('../../config/database');

module.exports = {
    getCategory: (req, res) => {
        var sql = "SELECT * FROM CategoryProduct";
        var query = connection.query(sql, (err, result) =>{
            if(err) throw err;
            res.json({
                count: result.length,
                next: null,
                previous: null,
                results: result
            });
        });
    },
    addCategory: (req, res) =>{
        var data = {
            nameCategory: req.body.nameCategory,
            descriptionCategory: req.body.descriptionCategory
        };
        var sql = "INSERT INTO CategoryProduct SET ?";
        var query = connection.query(sql, data,  (err, result) => {
            if(err) throw err;
            res.json({
                reponse: "Add successfully",
                results: result
            });
        });
    },
    getCategoryById: (req, res) => {
        var cateId = req.params.id;
        var sql = "SELECT * FROM CategoryProduct WHERE idCategoryProduct=" + cateId;
        var query = connection.query(sql, (err, result) => {
            if(err) throw err;
            res.json({
                count: result.length,
                status: 200,
                error: err,
                results: result
            });
        });
    }
};