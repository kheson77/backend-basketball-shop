const router = require('express').Router();
const { checkToken } = require("../../auth/token_validation");

const {
    getCart, addCart, getCartById, deleteCart, getCartByUserId, updateCountCart
} = require('./cart.controller');

router.get('/', checkToken, getCart);
router.post('/', checkToken, addCart);
router.get('/byIdUser/:idUser', checkToken, getCartByUserId);
router.get('/:id', checkToken, getCartById);
router.delete('/:id', checkToken, deleteCart);
router.patch('/:id', checkToken, updateCountCart);


module.exports = router;