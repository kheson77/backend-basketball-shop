const connection = require('../../config/database');

module.exports = {
    getCart: (req, res) => {
        var sql = "SELECT * FROM Cart";
        var query = connection.query(sql, (err, result) =>{
            if(err) throw err;
            res.json({
                count: result.length,
                next: null,
                previous: null,
                results: result
            });
        });
    },
    getCartById: (req, res) =>{
        var idCart = req.params.id;
        var sql = "SELECT * FROM Cart WHERE idCart=" + idCart;
        var query = connection.query(sql, (err, result) => {
            if(err) throw err;
            res.json({
                count: result.length,
                next: null,
                previous: null,
                results: result
            });
        });
    },
    getCartByUserId: (req, res) => {
        var idUser = req.params.idUser;
        var sql = "SELECT * FROM Cart WHERE idUser=" + idUser;
        var query = connection.query(sql, (err, result) => {
            if(err) throw err;
            res.json({
                count: result.length,
                next: null,
                previous: null,
                results: result
            });
        });
    },
    addCart: (req, res) => {
        var data = {
            idUser: req.body.idUser,
            idProduct: req.body.idProduct,
            idCategory: req.body.idCategory,
            nameProduct: req.body.nameProduct,
            thumbnailProduct: req.body.thumbnailProduct,
            priceProduct: req.body.priceProduct,
            quantity: req.body.quantity
        };
        var sql = "INSERT INTO Cart SET ?";
        var query = connection.query(sql, data, (err, result) => {
            if(err) throw err;
            res.json({
                reponse: "Add successfully",
                results: result
            });
        });
    },
    deleteCart: (req, res) => {
        var idCart = req.params.id;
        var sql = "DELETE FROM Cart WHERE idCart=" + idCart;
        var query = connection.query(sql, (err, result) => {
            if(err) throw err;
            res.json({
                reponse: "Delete successfully",
                results: result
            });
        })
    },
    updateCountCart: (req, res) => {
        var idCart = req.params.id;
        var data = {
            quantity: req.body.quantity
        };
        var sql = "UPDATE Cart SET ? WHERE idCart=" + idCart;
        var query = connection.query(sql, data, (err, result) => {
            if(err) throw err;
            res.json({
                response: "Update successfully",
                results: result
            });
        })
    }
}